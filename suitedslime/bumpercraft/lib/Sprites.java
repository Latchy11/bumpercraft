package suitedslime.bumpercraft.lib;

public class Sprites 
{
	public static final String SPRITE_SHEET_LOCATION = "/suitedslime/bumpercraft/art/";
	
	public static final String ITEM_SPRITE_SHEET  = "lbc_items.png";
	public static final String BLOCK_SPRITE_SHEET = "lbc_block.png";
}