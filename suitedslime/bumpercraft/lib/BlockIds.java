package suitedslime.bumpercraft.lib;

/**
 * BlockIds
 * 
 * Library containing the default values for mod related Block ids
 * 
 * @author SuitedSlime
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

public class BlockIds 
{
	public static int BLOCK_BLOCK = 2400;
}
