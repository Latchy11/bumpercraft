package suitedslime.bumpercraft.lib;

/**
 * Reference
 * 
 * General purpose library to contain mod related constants
 * 
 * @author SuitedSlime
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

public class Reference 
{
	/* General Mod related constants */
	public static final String MOD_ID = "BumperCraft";
	public static final String MOD_NAME = "BumperCraft";
	public static final String SERVER_PROXY_CLASS = "suitedslime.bumpercraft.core.proxy.CommonProxy";
	public static final String CLIENT_PROXY_CLASS = "suitedslime.bumpercraft.core.proxy.ClientProxy"; 
}
