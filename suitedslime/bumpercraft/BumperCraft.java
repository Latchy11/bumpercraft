package suitedslime.bumpercraft;

import suitedslime.bumpercraft.core.proxy.CommonProxy;
import suitedslime.bumpercraft.lib.Reference;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;

/**
 * BumperCraft
 * 
 * Main mod class for the Minecraft mod Bumper Craft
 * 
 * @author SuitedSlime
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = "0.0.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = false )

public class BumperCraft 
{
	@Instance(Reference.MOD_ID)
	public static BumperCraft instance;
	
	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
}
