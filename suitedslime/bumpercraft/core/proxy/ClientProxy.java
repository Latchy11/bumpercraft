package suitedslime.bumpercraft.core.proxy;

import suitedslime.bumpercraft.lib.Sprites;

import net.minecraftforge.client.MinecraftForgeClient;

/**
 * ClientProxy
 * 
 * Client specific functionality that cannot be put into CommonProxy
 * 
 * @author SuitedSlime
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

public class ClientProxy extends CommonProxy
{
	@Override
	public void initRenderingAndTextures()
	{
		MinecraftForgeClient.preloadTexture(Sprites.SPRITE_SHEET_LOCATION + Sprites.BLOCK_SPRITE_SHEET);
		MinecraftForgeClient.preloadTexture(Sprites.SPRITE_SHEET_LOCATION + Sprites.ITEM_SPRITE_SHEET);
	}
}
