package suitedslime.bumpercraft.core.proxy;

/**
 * CommonProxy
 * 
 * The common proxy class between client and server. Client proxy extends this
 * for further client specific functionality
 * 
 * @author SuitedSlime
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommonProxy 
{
	public void initRenderingAndTextures()
	{
		// Nothing here as the server doesn't render!
	}
}
